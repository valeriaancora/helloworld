<?php
/**
Ancora Helloworld
 */

/**
 * class Ancora_Helloworld_Block_Helloworld
 *
 * Hello World Block
 * @author Valeria Ancora <ancora.valeria@gmail.com>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Ancora_Helloworld_Block_Helloworld extends Mage_Core_Block_Template
{
    /**
     * isEnabled
     *
     * Returns true if the module is enabled to displayed.
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::helper('ancora_helloworld')->isEnabled();
    }

    /**
     * getMessage
     *
     * Returns the custom message, if the module is enabled.
     * @return string|boolean
     */
    public function getMessage()
    {
        if ($this->isEnabled()) {
            return Mage::helper('ancora_helloworld')->getConfigData('configuration/custom_message');
        }
        return false;
    }


    /**
     * getUrl
     *
     * Returns the Hello World URL.
     * @return string
     */
    public function getHelloworldUrl()
    {
        return Mage::getBaseUrl() . 'helloworld';
    }
}