<?php
/**
Ancora Helloworld
 */

/**
 * class Ancora_Helloworld_IndexController
 *
 * Main controller
 * @author Valeria Ancora <ancora.valeria@gmail.com>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Ancora_Helloworld_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}